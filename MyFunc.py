import matplotlib.pyplot as plt
import time
import copy
def ti(sttime,endtime):
    return(endtime-sttime)
def Umnoz(MatrA,MatrB,nachRazm,h):
    MatrC=[]
    stroC=[]
    MatrA=copy.deepcopy(MatrA)
    MatrB=copy.deepcopy(MatrB)
    r=0
    sttime=time.time()

    while r<h:
        i=0
        while i<h:
            j=0
            x=0
            while j<nachRazm:
                x+=(MatrA[r][j])*(MatrB[j][i])
                j+=1
            stroC.append(x)             
            i+=1            
        MatrC.append(copy.deepcopy(stroC))
        stroC.clear()
        r+=1
    endtime=time.time()
    print("умножение выполнено")
    t=ti(sttime,endtime)
    return(MatrC,t)
def graf(grx,gry1,gry2):
    plt.title("линейная зависимость времени выполнения от размерности матрицы")
    plt.xlabel("размерность матрицы, элементы")
    plt.ylabel("время выполнения,c")
    plt.grid()
    plt.plot(grx,gry1,label='y1 - float')
    plt.plot(grx,gry2,label= 'y2 - int')
    plt.legend()
    plt.show()
